do
    tbl = {}
    local SDA_PIN = 6 -- sda pin, GPIO12
    local SCL_PIN = 5 -- scl pin, GPIO14
    bh1750 = require("bh1750")
    bh1750.init(SDA_PIN, SCL_PIN)
    local work = function()
        print('Work Done!')
        bh1750 = nil
        package.loaded["bh1750"]=nil
        table.foreach(tbl,print)
    end
    bh1750.getlux(work, tbl)
    --bh1750.readdt(0x20, 2)
    tmr.create():alarm(10000,1, function()
        bh1750 = require("bh1750")
        bh1750.getlux(work, tbl)
    end)
end
