local M = {}
M.SDA = 6 -- sda pin, GPIO12
M.SCL = 5 -- scl pin, GPIO14
M.ADDR = 0x23
M.id = 0
M.l = 0
M.CMD = 0x10
M.int = false
local c
--[[
M.readdt = function(com, lngt)
    i2c.start(M.id)
    i2c.address(M.id, M.ADDR, i2c.TRANSMITTER)
    i2c.write(M.id, com)
    i2c.stop(M.id)
    i2c.start(M.id)
    i2c.address(M.id, M.ADDR,i2c.RECEIVER)
    
    tmr.create():alarm(200, 0, function()
        local c = i2c.read(M.id, lngt)
        i2c.stop(M.id)
        for i = 1, #c do
            print('byte '..i, string.format('0x%02X', c:byte(i)))
        end
        c = nil
    end)
end
--]]

function M.init(sda, scl, call)
    if sda then M.SDA = sda end
    if scl then M.SCL = scl end 
    i2c.setup(M.id, M.SDA, M.SCL, i2c.SLOW)
    print('Pins: SDA = '.. M.SDA..' SCL = '..M.SCL)
    M.int = true
    if call then call() end
end
M.read_data = function()
    i2c.start(M.id)
    i2c.address(M.id, M.ADDR, i2c.TRANSMITTER)
    i2c.write(M.id, M.CMD)
    i2c.stop(M.id)
    i2c.start(M.id)
    i2c.address(M.id, M.ADDR, i2c.RECEIVER)

    tmr.create():alarm(200, 0, function()
        c = i2c.read(M.id, 2)
        i2c.stop(M.id)
        M.l = math.floor((c:byte(1) * 256 + c:byte(2))/1.2)
        print('Lux:', M.l)
        if M.tbl then M.tbl.lux = M.l end 
        if M.call then M.call(M.l) end
    end)
end
function M.getlux(call, tbl)
    if call then M.call = call end
    if tbl then M.tbl = tbl end
    if (not M.int) then
        M.init(M.SDA, M.SCL, M.read_data)
    else
        M.read_data()
    end
end
return M